package geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CircleTest {
    
    @Test
    public void getTest(){
        Circle c = new Circle(3);
        assertEquals(3, c.getRadius(),0);
    }

    @Test 
    public void getAreaTest(){
        Circle c = new Circle(3);
        assertEquals(28.2743338823, c.getArea(),0.0000000001);
    }

    @Test
    public void equalsTest(){
        Circle c = new Circle(3);
        assertEquals("Radius: 3.0", c.toString());
    }

}
