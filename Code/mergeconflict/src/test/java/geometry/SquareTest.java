package geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SquareTest {
    @Test
    public void testDimensions(){
        Square ss = new Square(6);

        assertEquals(6, ss.getSide(), 0);
    }

    @Test
    public void testArea(){
        Square ss = new Square(6);

        assertEquals(36, ss.getArea(), 0);
    }

    @Test
    public void testString(){
        Square ss = new Square(6);
        
        assertEquals("This square has side dimension of " + ss.getSide(), ss.toString());
    }
}
