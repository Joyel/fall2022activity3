package geometry;

public class Shapes {
    public static void main(String[] args){
        Circle c = new Circle(3);
        Square theBox = new Square(6);
        System.out.println(c);
        System.out.println(theBox);
        System.out.println("Its area is " + theBox.getArea());
    }
}
