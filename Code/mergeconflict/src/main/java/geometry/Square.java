package geometry;

public class Square {
    private double side;

    public Square(double newSide){
        this.side = newSide;
    }

    public double getSide(){
        return this.side;
    }

    public double getArea(){
        return Math.pow(side,2);
    }

    public String toString(){
        return "This square has side dimension of " + this.side;
    }
}
