package geometry;

public class Circle {
    private double radius;

    public Circle(int input){
        this.radius=input;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        return this.radius*this.radius*Math.PI;
    }

    public String toString(){
        return "Radius: "+this.radius;
    }


}
